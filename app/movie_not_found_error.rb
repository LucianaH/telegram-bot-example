class MovieNotFoundError < StandardError
  def initialize(_msg = nil)
    super('Movie not found')
  end
end
