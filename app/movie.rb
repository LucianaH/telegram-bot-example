class Movie
  attr_reader :awards, :name

  NO_AWARDS = 'N/A'.freeze

  def initialize(name, awards)
    @name = name
    @awards = if awards == NO_AWARDS
                'Not available'
              else
                awards
              end
  end
end
