require "#{File.dirname(__FILE__)}/../app/movie_not_found_error"

class MovieSearcher
  API_URL = 'https://www.omdbapi.com/'.freeze
  ERROR_RESPONSE = 'False'.freeze

  def initialize(api_key)
    @api_key = api_key
  end

  def get_movie(movie_name)
    response = Faraday.get(API_URL, { t: movie_name, apikey: @api_key })
    response_json = JSON.parse(response.body)

    raise MovieNotFoundError unless movie_exists(response_json)

    awards = response_json['Awards']

    Movie.new(movie_name, awards)
  end

  def movie_exists(response_json)
    response_json['Response'] != ERROR_RESPONSE
  end
end
