require "#{File.dirname(__FILE__)}/../app/movie_searcher"

describe MovieSearcher do
  def omdb_stub
    api_omdb_response_body = {
      "Title": 'Titanic',
      "Awards": 'Won 11 Oscars, 126 wins & 83 nominations total'
    }

    stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Titanic')
      .with(
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent' => 'Faraday v2.7.4'
        }
      )
      .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
  end

  it 'returns movie Titanic with its awards when searching movie name Titanic' do
    omdb_stub

    movie = described_class.new(ENV['OMDB_API_KEY']).get_movie('Titanic')

    expect(movie.name).to eq 'Titanic'
    expect(movie.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
  end
end
